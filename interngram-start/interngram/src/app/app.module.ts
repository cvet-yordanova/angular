import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {AuthComponent} from './auth/auth.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { PostsListComponent } from './posts/posts-list/posts-list.component';
import { PostsSingleComponent } from './posts/posts-list/posts-single/posts-single.component';
import { PostsEditComponent } from './posts/posts-edit/posts-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {MatIconModule} from '@angular/material';
import { PostsImageComponent } from './posts/posts-list/posts-image/posts-image.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AuthComponent,
    LoadingSpinnerComponent,
    PostsListComponent,
    PostsSingleComponent,
    PostsEditComponent,
    PostsImageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    InfiniteScrollModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
