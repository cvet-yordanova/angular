export class Meta {
    url: string;
    alt: string;
    thumbnail: string;
    videoLinkEmbed: string;
    videoLink: string;

    constructor(url: string,
         alt: string, 
         thumbnail: string,
         videoLinkEmbed: string,
         videoLink: string){
        this.url = url;
        this.alt = alt;
        this.thumbnail = thumbnail;
        this.videoLinkEmbed = videoLinkEmbed;
        this.videoLink = videoLink;
    }
}