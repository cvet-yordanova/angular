import { Meta } from "./meta.model";

export class Post {
    _id: number;
    type: string;
    title: string;
    date: string;
    meta: Meta;
    
    constructor(type: string, title: string, meta: Meta){
        this.type = type;
        this.title = title;
        this.date = new Date().toString();

        this.meta = meta;
    }

    get id() {
        return this._id;
    }


}