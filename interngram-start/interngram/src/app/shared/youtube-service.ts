import { HttpClient, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
})
export class YoutubeService {

    url = 'https://www.googleapis.com/youtube/v3/videos?part=id&id=b_bJQgZdjzo&key=';
    youtubeApi = 'AIzaSyAm4V9UlGAOlFqdYXGN5hZOoMCCHHFTLcY';

    constructor(private http: HttpClient) {

    }
    getVideoId(input: string) {
        let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        let match = input.match(regExp);
        return (match && match[7].length == 11) ? match[7] : '';
    }

    checkIfVideoExists(videoId) {
        return this.http.get<any>(`https://www.googleapis.com/youtube/v3/videos?part=id&id=${videoId}&key=${this.youtubeApi}`);
    }


    getDefaultYoutubeImg(){
        return 'https://www.tubefilter.com/wp-content/uploads/2018/12/youtube-logo.jpg';
    }

    getVideoThumbnail(videoId) {
        return `https://img.youtube.com/vi/${videoId}/mqdefault.jpg`;
    }

    getVideoUrlWatch(videoId) {
        return `https://www.youtube.com/watch?v="${videoId}`;
    }

    getVideoUrlEmbed(videoId) {
        return `https://www.youtube.com/embed/${videoId}`;
    }

}