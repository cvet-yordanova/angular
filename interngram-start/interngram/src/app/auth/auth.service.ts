import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Subject, BehaviorSubject } from 'rxjs';
import { catchError, tap } from "rxjs/operators";
import { Injectable } from '@angular/core';
import { User } from './user.model';
import { Router } from '@angular/router';

export interface AuthResponseData {
    kind: string;
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registeredId?: boolean;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
    user = new BehaviorSubject<User>(null);
    private tokenExpirationTimer:any;
    constructor(private http: HttpClient, private router: Router) { }

    signin(email: string, password: string) {
        return this.http.post<AuthResponseData>(
            'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyC34zIgLB5tlQ6DnivxfKUILpn2GNN-Rj4',
            {
                email: email,
                password: password,
                returnSecureToken: true
            }
        ).pipe(
            catchError(this.handleError), tap(respData => {
                this.handleAuthentication(
                    respData.email,
                    respData.localId,
                    respData.idToken,
                    +respData.expiresIn
                )

            })
        )
    }

    signup(email: string, password: string) {
        return this.http.post<AuthResponseData>(
            'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyC34zIgLB5tlQ6DnivxfKUILpn2GNN-Rj4',
            {
                email: email,
                password: password,
                returnSecureToken: true
            }
        ).pipe(
            catchError(this.handleError), tap(respData => {
                this.handleAuthentication(
                    respData.email,
                    respData.localId,
                    respData.idToken,
                    +respData.expiresIn
                )

            })
        )
    }

    private handleError(errorRes: HttpErrorResponse) {
        let errorMessage = "An unkonwn error occured!";
        if (!errorRes.error || !errorRes.error.error) {

        }

        switch (errorRes.error.error.message) {
            case 'EMAIL_EXISTS':
                errorMessage = 'This email already exists!';
                break;
            case 'EMAIL_NOT_FOUND':
                errorMessage = 'This email does not exists!';
                break;
            case 'INVALID PASSWORD':
                errorMessage = 'This password is incorrect!'
                break;

        }
        return throwError(errorMessage);
    }

    autoLogin() {
        const userData: {
            email: string;
            id: string;
            _token: string;
            _tokenExiprationDate: string
        } = JSON.parse(localStorage.getItem('userData'));
        if(!userData) {
            return;
        }

        const loadedUser = new User(
            userData.email,
            userData.id,
            userData._token,
            new Date(userData._tokenExiprationDate)
        )

        if(loadedUser.token) {
            this.user.next(loadedUser);
            const expirationDuration = 
            new Date(userData._tokenExiprationDate).getTime()-
            new Date().getTime();
            this.autoLogout(expirationDuration);
        }
    }
    autoLogout(expirationDuration: number){
        this.tokenExpirationTimer =  setTimeout(() => {
             this.logout();
         }, expirationDuration)
     }
    logout(){
        this.user.next(null);
         this.router.navigate(['/auth']);
        localStorage.removeItem('userData');
        if(this.tokenExpirationTimer){
            clearTimeout(this.tokenExpirationTimer);
        }
        this.tokenExpirationTimer = null;
    }


    private handleAuthentication(email: string, userId: string, token: string, expiresIn: number) {
        const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
        const user = new User(email, userId, token, expirationDate);
        this.user.next(user);
         this.autoLogout(expiresIn * 1000);
        localStorage.setItem('userData', JSON.stringify(user));
    }
}