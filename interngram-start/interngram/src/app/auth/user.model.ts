export class User {
    constructor(
        public email: string,
        public id: string,
        private _token: string,
        private _tokenExpirationDat
    ){}

    get token(){
        if(!this._tokenExpirationDat || new Date() > this._tokenExpirationDat){
            return null;
        }

        return this._token;
    }
}