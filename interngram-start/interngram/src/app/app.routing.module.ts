import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { NgModule } from '@angular/core';
import { PostsListComponent } from './posts/posts-list/posts-list.component';
import { PostsEditComponent } from './posts/posts-edit/posts-edit.component';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'posts' },
    {
        path: 'posts', component: PostsListComponent, 
        // children: [
            // { path: '', component: PostsListComponent },
            // { path: 'new', component: RecipeEditComponent },
            // {
            //     path: ':id/edit',
            //     component: PostsEditComponent
            // }
        // ]
        
    },
    {path: 'posts/:id/edit', component: PostsEditComponent},
    {path: 'posts/add', component: PostsEditComponent}
    // { path: 'auth', component: AuthComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }