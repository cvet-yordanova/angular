import { Component, OnInit, OnChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Post } from '../../models/post.model';
import { PostsService } from '../posts.service';
// import { Route } from '@angular/compiler/src/core';

import { BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {

  posts$: Observable<Array<Post>>;
  postsNormal = new BehaviorSubject([]);

  batch = 2;
  lastKey = '';
  finished = false;
  page = 1;
  lastPage = false;
  showDetails = false;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.posts$ = this.postsService.getAllPosts();

    this.postsService.getPostsScroll(this.page).subscribe(data => {

      if (this.lastPage) {
        return;
      }

      if (data.length < 10) {
        this.lastPage = true;
      }

      if (data.length >= 10) {
        this.page++;
      }

      this.postsNormal.next(_.concat(this.postsNormal.getValue(), data));

    });

    this.postsService.postsChanged.subscribe(() => {

      if(this.lastPage) {
        this.postsService.getAllPosts().subscribe(data => {
          this.postsNormal.next(data);
        });
      }


    });

  }

  onScroll() {
    this.postsService.getPostsScroll(this.page++).subscribe(data => {

      if (this.lastPage) {
        console.log('last page?');
        return;
      }

      if (data.length < 10) {
        this.lastPage = true;
      }

      if (data.length >= 10) {
        this.page++;
      }

      this.postsNormal.next(_.concat(this.postsNormal.getValue(), data));

    })
  }

  opened(e) {
    console.log(e)
    this.showDetails = true;

  }

  closed() {
    this.showDetails = false;
  }

}
