import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-posts-image',
  templateUrl: './posts-image.component.html',
  styleUrls: ['./posts-image.component.css']
})
export class PostsImageComponent implements OnInit {

  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  // onClose(){
  //   this.close.emit(null);
  // }

  onClose(){
    console.log('clicked closed');
    this.close.emit(null);
  }

}
