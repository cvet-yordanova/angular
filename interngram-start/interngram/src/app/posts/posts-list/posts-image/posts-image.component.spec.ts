import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsImageComponent } from './posts-image.component';

describe('PostsImageComponent', () => {
  let component: PostsImageComponent;
  let fixture: ComponentFixture<PostsImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
