import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../../../models/post.model';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from '../../posts.service';

@Component({
  selector: 'app-posts-single',
  templateUrl: './posts-single.component.html',
  styleUrls: ['./posts-single.component.css']
})
export class PostsSingleComponent implements OnInit {

  @Input() post: Post;

  // @Output() open: EventEmitter<any> = new EventEmitter();
  // @Output() close: EventEmitter<any> = new EventEmitter();
  @Output() open: EventEmitter<any> = new EventEmitter();

  constructor(private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
  }

  onEditPost() {
    this.router.navigate([this.post.id, 'edit'], { relativeTo: this.route });
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onDeletePost() {
    let id = +this.post.id;
    if(confirm('Are you sure you want to delete this?')) {
      this.postsService.deletePost(id);
    } 
  }

  onView(){
    console.log('clicked');
    this.open.emit(this.post);
  }

}
