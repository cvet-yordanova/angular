import { Component, OnInit, SecurityContext, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PostsService } from '../posts.service';
import { Post } from '../../models/post.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { YoutubeService } from '../../shared/youtube-service';
import { Meta } from '../../models/meta.model';

@Component({
    selector: 'app-posts-edit',
    templateUrl: './posts-edit.component.html',
    styleUrls: ['./posts-edit.component.css']
})
export class PostsEditComponent implements OnInit {

    id: number;
    postForm: FormGroup;
    post: Post;
    editMode: boolean = false;
    videoId: string;
    videoLink: string;
    videoLinkEmbed: string;
    imageLink: string;
    thumbImage: string;

    @ViewChild('videoElement') videoElement: ElementRef;
    @ViewChild('type') typeElement: ElementRef;
    @ViewChild('url') linkElement: ElementRef;
    @ViewChild('imgPreview') imgPreview: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private postsService: PostsService,
        private router: Router,
        private youtubeService: YoutubeService
    ) { }

    ngOnInit() {

        this.route.params.subscribe((params: Params) => {
            this.id = +params['id'];
            this.editMode = params['id'] != null;
            this.initForm()
        });

    }

    private initForm() {

        let postType = '';
        let postTitle = '';
        let postMetaUrl = '';
        let postMetaAlt = '';


        if (this.editMode) {
            this.postsService.getPostById(this.id).subscribe((data) => {
                this.post = data;
                console.log(this.post);

            }, error => {
                console.log(error);
            }, () => {
                postTitle = this.post.title;
                postMetaUrl = this.post.meta.url ? this.post.meta.url : '';
                postMetaAlt = this.post.meta.alt ? this.post.meta.alt : '';
                postType = this.post.type;

                this.createFormGroup(postType, postTitle, postMetaUrl, postMetaAlt)
                return;
            });
        }

        this.createFormGroup(postType, postTitle, postMetaUrl, postMetaAlt);

    }

    private createFormGroup(postType: string, postTitle: string, url: string, alt?: string) {
        this.postForm = new FormGroup({
            title: new FormControl(postTitle, Validators.required),
            type: new FormControl(postType, Validators.required),
            postMetaUrl: new FormControl(url),
            postMetaAlt: new FormControl(alt)
        });
    }

    onCancel() {
        // this.router.navigate(['./'], { relativeTo: this.route });
        this.router.navigate(['posts']);
    }

    onSubmit() {

        if (this.editMode) {
            this.postsService.updatePost(this.id, this.postForm.value);
        } else {
            this.updateLinks();
            //todo
            let formValue = this.postForm.value;
            let meta = new Meta(
                formValue.metaUrl,
                formValue.alt ? formValue.alt : null,
                this.thumbImage ? this.thumbImage : null,
                this.videoLinkEmbed ? this.videoLinkEmbed : null,
                this.videoLink ? this.videoLink : null
            );


            let newPost = new Post(formValue.type,
                formValue.title, meta);

            this.postsService.createPost(newPost);
        }
        this.router.navigate(['posts']);
    }


    onKey(event: any) { // without type info

    }

    private updateLinks() {

        let linkValueNative = this.linkElement.nativeElement;

        switch (this.typeElement.nativeElement.value) {
            case 'IMAGE':
                this.thumbImage = this.linkElement.nativeElement.value;
                break;
            case 'VIDEO':
                //todo
                this.videoId = this.youtubeService.getVideoId(linkValueNative.value);
                this.youtubeService.checkIfVideoExists(this.videoId).subscribe(data => {
                    console.log(data.pageInfo.totalResults);
                    if (data.pageInfo.totalResults > 0) {
                        this.imageLink = this.youtubeService.getVideoThumbnail(this.videoId);
                        this.videoLink = this.youtubeService.getVideoUrlWatch(this.videoId);
                        this.videoLinkEmbed = this.youtubeService.getVideoUrlEmbed(this.videoId);

                    } else {
                        this.videoLink = this.youtubeService.getDefaultYoutubeImg();
                        this.videoLinkEmbed = null;
                    }
                });
                break;
            case 'LINK':
                break;
            default: this.thumbImage = null;
        }

    }

}
