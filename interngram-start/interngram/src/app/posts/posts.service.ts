import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { Post } from "../models/post.model";


const getAllPosts = 'http://localhost:3000/posts';
const getSinglePost = 'http://localhost:3000/posts/';

@Injectable({
    providedIn: 'root'
})
export class PostsService {

    postsChanged = new Subject<string>();
    posts: Post[];
    activePost: Post;

    constructor(private http: HttpClient) {

    }

    getPostsScroll(page) {
        // return this.http.get<Array<Post>>(getAllPosts + `?_start=${start}&_end=${end}&_sort=date&_order=asc`);
        return this.http.get<Array<Post>>(getAllPosts + `?_page=${page}&_sort=date&_order=asc`);
    }

    getAllPosts(): Observable<Array<Post>> {
        // return this.http.get<Array<Post>>(getAllPosts);
        return this.http.get<Array<Post>>(getAllPosts + `?&_sort=date&_order=asc`);

    }


    getPostById(id: number): Observable<Post> {
        return this.http.get<Post>(getSinglePost + id);
    }

    getPostsArray() {
        return this.posts;
    }

    updatePost(id: number, data) {
        console.log(data);
        this.getPostById(id).subscribe(resultPost => {
            resultPost.title = data.title;
            resultPost.type = data.type;
            resultPost.meta.url = data.postMetaAlt;
            resultPost.meta.alt = data.postMetaUrl;
            this.http.patch(getSinglePost + id, resultPost).subscribe(result => {
                this.postsChanged.next('changed');
            });
        });

    }

    createPost(post: Post) {
 
        this.http.post(getAllPosts, post).subscribe(result => {
            this.postsChanged.next('changed');       
        });
    }

    deletePost(id: number) {
        this.http.delete(getAllPosts + '/' + id).subscribe((result) => {
            this.postsChanged.next('deleted');            
        });
    }

}